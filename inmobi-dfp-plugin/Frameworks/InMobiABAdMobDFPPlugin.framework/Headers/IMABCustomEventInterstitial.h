//
//  IMABCustomEventInterstitial.h
//  InMobiABAdMobDFPPlugin
//
//  Created by Vikas Kumar Jangir on 23/06/19.
//  Copyright © 2019 InMobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
NS_ASSUME_NONNULL_BEGIN

@interface IMABCustomEventInterstitial : NSObject <GADCustomEventInterstitial>

@end

NS_ASSUME_NONNULL_END
