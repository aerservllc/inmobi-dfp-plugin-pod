//
//  IMABCustomEventBanner.h
//  InMobiABAdMobDFPPlugin
//
//  Created by Vikas Kumar Jangir on 19/06/19.
//  Copyright © 2019 InMobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
NS_ASSUME_NONNULL_BEGIN

@interface IMABCustomEventBanner : NSObject <GADCustomEventBanner>

@end

NS_ASSUME_NONNULL_END
