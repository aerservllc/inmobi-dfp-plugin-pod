//
//  InMobiABAdMobDFPPlugin.h
//  InMobiABAdMobDFPPlugin
//
//  Created by Vikas Kumar Jangir on 19/06/19.
//  Copyright © 2019 InMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for InMobiABAdMobDFPPlugin.
FOUNDATION_EXPORT double InMobiABAdMobDFPPluginVersionNumber;

//! Project version string for InMobiABAdMobDFPPlugin.
FOUNDATION_EXPORT const unsigned char InMobiABAdMobDFPPluginVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <InMobiABAdMobDFPPlugin/PublicHeader.h>

#import <InMobiABAdMobDFPPlugin/IMABCustomEventBanner.h>
#import <InMobiABAdMobDFPPlugin/IMABCustomEventInterstitial.h>
