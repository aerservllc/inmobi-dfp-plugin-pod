### inmobi-mp-plugin.podspec

Pod::Spec.new do |s|
  s.name                  = 'inmobi-dfp-plugin'
  s.version               = '1.0.0.820'
  s.summary               = 'InMobi Audience Bidder - DFP Support'
  s.description           = <<-DESC
InMobi Audience Bidder - DFP Support. Copyright 2019 InMobi, all rights reserved.
                            DESC
  s.homepage              = 'https://bitbucket.org/aerservllc/inmobi-dfp-plugin-pod'
  s.license               = { :type => 'MIT', :text => <<-LICENSE
                                Copyright (c) 2018 InMobi <sdk-dev-support@inmobi.com>

                                Permission is hereby granted, free of charge, to any person obtaining a copy
                                of this software and associated documentation files (the "Software"), to deal
                                in the Software without restriction, including without limitation the rights
                                to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
                                copies of the Software, and to permit persons to whom the Software is
                                furnished to do so, subject to the following conditions:

                                The above copyright notice and this permission notice shall be included in
                                all copies or substantial portions of the Software.

                                THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
                                IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
                                FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
                                AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
                                LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
                                OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
                                THE SOFTWARE.
                              LICENSE
                            }
  s.author                = { "InMobi" => "sdk-dev-support@inmobi.com" }
   s.source               = { :git => 'https://bitbucket.org/vikasjangir/inmobi-dfp-plugin-pod.git', :tag => s.version.to_s }
  s.platform              = :ios, '9.0'
  s.ios.deployment_target = '9.0'
  s.default_subspecs      = 'InMobiABAdMobDFPPlugin'

  s.subspec 'InMobiSDK' do |imss|
    imss.dependency            'mopub-ios-sdk' 
    imss.source_files          = 'inmobi-dfp-plugin/Frameworks/InMobiSDK.framework/Headers/*.{h,m}'
    imss.public_header_files   = 'inmobi-dfp-plugin/Frameworks/InMobiSDK.framework/Headers/*.h'
    imss.vendored_frameworks   = 'inmobi-dfp-plugin/Frameworks/InMobiSDK.framework'
    imss.preserve_paths        = 'inmobi-dfp-plugin/Frameworks/InMobiSDK.framework'
  end

  s.subspec 'InMobiABAdMobDFPPlugin' do |imabmpss|
    imabmpss.dependency            'Google-Mobile-Ads-SDK' 
    imabmpss.dependency            'inmobi-dfp-plugin/InMobiSDK'
    imabmpss.source_files          = 'inmobi-dfp-plugin/Frameworks/InMobiABAdMobDFPPlugin.framework/Headers/*.{h,m}'
    imabmpss.public_header_files   = 'inmobi-dfp-plugin/Frameworks/InMobiABAdMobDFPPlugin.framework/Headers/*.h'
    imabmpss.vendored_frameworks   = 'inmobi-dfp-plugin/Frameworks/InMobiABAdMobDFPPlugin.framework'
    imabmpss.preserve_paths        = 'inmobi-dfp-plugin/Frameworks/InMobiABAdMobDFPPlugin.framework'
  end  
end